import React from 'react';
import { StyleSheet, Text,Alert, View, TouchableHighlight,Await,AsyncStorage, Modal,TabBarIOS, TextInput, Button } from 'react-native';

export default class App extends React.Component {

     constructor(props) {
     super(props);
      this.state = {
        selectedTab: 0,
        text: '',
        latitude: '',
        longitude: '',
        error: '',
        modalVisible: false,
        notes:''
      };
 this.persistData = this.persistData.bind(this);
 this.clearData = this.clearData.bind(this);
  
}
persistData(){
     let notes = this.state.notes
     let latitude = this.state.latitude
     let longitude = this.longitude
     AsyncStorage.setItem('notes', notes).done();
     this.setState({notes: notes, persistednotes: notes, latitude: this.latitude, longitude: this.longitude})
  }

  check(){

    AsyncStorage.getItem('notes').then((notes) => {
        this.setState({notes: notes, persistednotes: notes})
    })

       AsyncStorage.getItem('latitude').then((latitude) => {
        this.setState({latitude: latitude, persistedlatitude: latitude})
    })

  }

  clearData(){
    AsyncStorage.clear();
    this.setState({ persistednotes: ''})
 
  }

  componentWillMount(){
    this.check();
  }

componentDidMount() {

    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
    );
  }

   componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  handleTabPress(tab) {
  this.setState({ selectedTab: tab })
}

  render() {
    return (
      <View style={styles.container}>
        <TabBarIOS>
        <TabBarIOS.Item
          icon={require('./assets/notes.png')}
          title="View Notes"
          selected={this.state.selectedTab === 0}
          onPress={this.handleTabPress.bind(this, 0)}
        >
      
        <View style={styles.view}>
            <Text style={styles.heading}>last Added Note</Text>
             <View>
          
                <Text style={styles.addedNote}>Note: {this.state.persistednotes}</Text>
                <Text>{this.state.latitude}</Text>
                <Text>{this.state.longitude}</Text>
         
            </View>

          </View>
        
        </TabBarIOS.Item>

        <TabBarIOS.Item
          title="Add Notes"
          icon={require('./assets/addNote.png')}
          selected={this.state.selectedTab === 1}
          onPress={this.handleTabPress.bind(this, 1)}
        >
          <View >
            <View style={{marginTop: 22}}>
              <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {alert("Modal has been closed.")}}
                >
         <View style={styles.view}>
          <View>
               <Text style={styles.heading}>Add Notes</Text>
               <Text>Notes</Text>
                <TextInput
                value={this.state.notes}
                onChangeText={(text) => this.setState({notes: text})}
                style={styles.input} />

                <Text style={styles.text}>My latitude : {this.state.latitude} </Text>
                <Text style={styles.text}>My longitude : {this.state.longitude} </Text>


        <TouchableHighlight
          style={styles.button}
          onPress={this.persistData}
          underlayColor="white">
          <Text style={styles.buttonText}> Save </Text>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.button}
          onPress={this.clearData}
          underlayColor="white">
          <Text style={styles.buttonText}> Clear Db </Text>
        </TouchableHighlight>
            <TouchableHighlight
               style={styles.button}
                underlayColor="white" onPress={() => {
              this.setModalVisible(!this.state.modalVisible)
            }}>
              <Text style={styles.buttonText}>Hide Modal</Text>
            </TouchableHighlight>

          </View>
         </View>
        </Modal>

        <TouchableHighlight
         style={styles.btnAdd}
          onPress={() => {
          
          this.setModalVisible(true)
        }}>
          <Text  style={styles.buttonText}>Add Notes</Text>
        </TouchableHighlight>

      </View>
            
          </View>
          
        </TabBarIOS.Item>
</TabBarIOS>
    </View>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
  title: {
    fontSize: 20
  },
  addedNote: {
    marginBottom:20
  },
  heading:{
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom:30
  
    
  },
  button: {
    height: 35,
    flexDirection: 'row',
    backgroundColor: '#006289',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  
  
  },
  btnAdd :{
    marginTop:300,
    padding:25,
     height: 75,
    flexDirection: 'row',
    backgroundColor: '#006289',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  btnHide:{
     backgroundColor: '#006289',
     height: 45,
     borderWidth: 1,
     borderRadius: 8,
  },

  buttonText: {
    fontSize: 18,
    color: '#fff',
    alignSelf: 'center'
  },
  input: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 8,
    color: 'black',
    marginBottom: 10
  },
  view:{
    marginTop:50,
  }
});
